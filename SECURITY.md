# Security<!-- omit in toc -->

The contents of this file are determined by the Label owner of _🛡️ Security_.

See this product's [`security.txt`](https://securitytxt.org/) at [`.well-known/security.txt`](.well-known/security.txt).
The `security.txt` must also be hosted in the documents root of any website we host.
