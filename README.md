# Standard product template<!-- omit in toc -->

- [Features](#features)
  - [`Default.code-profile`](#defaultcode-profile)
  - [`k8s/`](#k8s)
  - [`.containerignore` and `k8s/*/Containerfile.*`](#containerignore-and-k8scontainerfile)
  - [`.editorconfig`](#editorconfig)
  - [`.gitattributes`](#gitattributes)
  - [`.gitignore`](#gitignore)
  - [`.gitlab-ci.yml`](#gitlab-ciyml)
  - [`.lefthook.yml`](#lefthookyml)
  - [`.markdownlint.json`](#markdownlintjson)
  - [`.mega-linter.yml`](#mega-linteryml)
  - [`.prettierignore` and `.prettierrc.json`](#prettierignore-and-prettierrcjson)
  - [`v8rrc.yml`](#v8rrcyml)
  - [`.vale.ini` and `styles/`](#valeini-and-styles)
  - [`lychee.toml`](#lycheetoml)
  - [Legend](#legend)
- [To install](#to-install)
  - [To create a new product based on this template](#to-create-a-new-product-based-on-this-template)
- [To use](#to-use)
  - [To run checkers locally](#to-run-checkers-locally)
    - [Pre-push checking through Lefthook](#pre-push-checking-through-lefthook)
    - [Fixing through Lefthook](#fixing-through-lefthook)
    - [Checking through MegaLinter](#checking-through-megalinter)

## Features

The features are too many to list, but this section will relate concrete files in the template to functionality.

### `Default.code-profile`

A [Visual Studio Code profile](https://code.visualstudio.com/docs/editor/profiles) that includes optimal settings and extensions.
Language-specific Visual Studio Code profiles are available as well, e.g., [`python/Python.code-profile`](python/Python.code-profile).
All settings and extensions of the Default profile should be applied to all profiles, and the language-specific profiles should extend this basis.

⚠️ Running Visual Studio Code as Administrator will likely break the filesystem permissions of this directory.

### `k8s/`

Files for deploying containers with Kubernetes.

See [Learn Kubernetes basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/).

### `.containerignore` and `k8s/*/Containerfile.*`

See [`Containerfile`](https://github.com/containers/common/blob/main/docs/Containerfile.5.md).

See [`.containerignore`/`.dockerignore`](https://docs.podman.io/en/stable/markdown/podman-build.1.html#containerignore-dockerignore).

### `.editorconfig`

Editors use this to save text files in a standardized way.

See [EditorConfig](https://editorconfig.org/).

### `.gitattributes`

This instructs Git to deal with files in the best/expected a cross-Operating System way.

See [Configuring Git to handle line endings](https://docs.github.com/en/get-started/getting-started-with-git/configuring-git-to-handle-line-endings#per-repository-settings).

See, more generally, [Git - Configuration advice](https://san-mai.gitlab.io/snippets/Git.html#configuration-advice).

### `.gitignore`

This instructs Git to not detect changes in some files by default according to the team’s best practices.
For example, this avoid committing files with secrets or person-specific content.

See [Ignoring files](https://docs.github.com/en/get-started/getting-started-with-git/ignoring-files).

### `.gitlab-ci.yml`

See [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/index.html).

### `.lefthook.yml`

See [Lefthook](https://github.com/evilmartians/lefthook#readme).

Lefthook enables the various tools that check adherence to best practices and validity of your state of the repository as described in the other sections to run on your own computer, before you send your commits to GitLab (more generally, push commits to the Git remote).
In addition, its use helps to run the same tools in an identical configuration under GitLab CI/CD.

### `.markdownlint.json`

Markdownlint checks the content and structure of your Markdown documents for adherence to best practices.

See [markdownlint](https://github.com/DavidAnson/markdownlint#readme).

### `.mega-linter.yml`

MegaLinter extensively detects and processes use of tools, data formats and source code languages and scans them for adherence to best practices (‘linting’).
It includes [tens of such tools](https://megalinter.io/latest/supported-linters/) and run them when applicable (except those disabled, as specified in this configuration file).

See [MegaLinter by OX Security](https://megalinter.io/latest/).

### `.prettierignore` and `.prettierrc.json`

Prettier ensures a consistent and best-practice formatting and style of a wide range source code and data file types.

See [Why Prettier?](https://prettier.io/docs/en/why-prettier).

### `v8rrc.yml`

V8r checks whether common data/configuration files have been filled out in accordance with their schema, avoiding ineffective and/or invalid data.

See [v8r](https://github.com/chris48s/v8r#readme).

### `.vale.ini` and `styles/`

Vale checks written natural language (prose) for style, for example on clarity, brevity and inclusive language.

See [Vale.sh - A linter for prose](https://vale.sh/).

### `lychee.toml`

Lychee checks hyperlinks, URLs and other URIs across a wide range of source code and data files for availability.
This avoids so-called ‘dead hyperlinks’ and HTTP 404 and other errors for visitors directed to these addresses.

See [Welcome to lychee!](https://lychee.cli.rs/#/?id=main).

### Legend

In particular within the GitLab CI/CD files, icons are used for brevity and visual distinction among impactful jobs (to avoid unintentionally triggering some job).
Their definition is:

| Symbol | Definition                                                     |
| ------ | -------------------------------------------------------------- |
| 🧱     | Builder (container image)                                      |
| ⭐     | Acceptance (Kubernetes cluster)                                |
| ⭐ ⭐  | Production (Kubernetes cluster or container image)             |
| 🧑‍🔬     | Development (container image)                                  |
| 👶🏻     | Create related Kubernetes Resources                            |
| 👶🏻👶🏻   | Create container Kubernetes Namespace                          |
| ☠️     | Destroy related Kubernetes Resources                           |
| ☠️☠️   | Delete containing Kubernetes Namespace                         |
| 🪂     | Deployment of related Kubernetes Resources                     |
| 📡     | Data transfer feature (add-on to related Kubernetes resources) |

## To install

Using all the tools that this template configures only requires local installation on a development computer if you want to run checks and builds locally, as all functionality is covered with the [GitLab CI/CD pipeline](.gitlab-ci.yml), alternatively.
When you follow the [AIM Tools](https://gitlab.com/han-aim/aimtools) setup steps, you’re ready to run all tools locally.

⚠️ This template can only be used successfully (by non-experts) if you completely adhere to the prescriptions (referenced) in the documentation of AIM Tools.

### To create a new product based on this template

See the [AIM Tools](https://gitlab.com/han-aim/aimtools#to-lay-the-groundwork-for-a-new-git-based-work-product).

## To use

### To run checkers locally

Please run the below commands with the Git repository working copy’s root directory as working directory.
This applies to any Git repository of each product based on this template separately.

#### Pre-push checking through Lefthook

The GitLab CI/CD job in [`.gitlab-ci/product/check-lefthook-pre-push-ci.gitlab-ci.yml`](.gitlab-ci/product/check-lefthook-pre-push-ci.gitlab-ci.yml) references the checker manager [Lefthook](https://github.com/evilmartians/lefthook), which is to be run before every Git push, as well as during CI runs.
Lefthook manages most of the checkers configured in this template.
It can be run locally, as follows.

- [Install it locally as a Git pre-push hook using Lefthook](https://github.com/evilmartians/lefthook/blob/master/docs/usage.md#lefthook-install), so that the checks are run before you push something to a Git remote.

- Independent of the previous bullet point, to run the Lefthook checks manually at any time, you may issue:

  ```sh
  pdm run -- lefthook run pre-push-manually
  ```

Append `--all-files` to the command line to act on all files, not just those about to be pushed.

You can override an installed Git hook using the `--no-verify` argument to your Git command invocation, e.g., like so:

```sh
git push --no-verify
```

#### Fixing through Lefthook

```sh
pdm run -- lefthook run pre-push-manually-fix
```

#### Checking through MegaLinter

The GitLab CI/CD job in [`.gitlab-ci/check-megalinter.gitlab-ci.yml`](.gitlab-ci/check-megalinter.gitlab-ci.yml) defines [MegaLinter](https://megalinter.io/latest/).
MegaLinter manages some of the checkers configured in this template, that are complementary to the Lefthook-managed checkers.
MegaLinter’s main purpose is to dynamically detect and quality check newly introduced languages/file types during CI, since it dynamically selects checkers.
MegaLinter can also be run explicitly locally, like so:

```sh
lefthook run megalinter
```
