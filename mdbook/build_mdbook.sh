#!/bin/sh
set -eux
# TODO: Use aimtools product revision.
revision="rev. $(git rev-parse --short HEAD)"
edition="${CI_COMMIT_TAG:-$revision}"
MDBOOK_BOOK__TITLE="${MDBOOK_BOOK__TITLE:?} ($edition)"
export MDBOOK_BOOK__TITLE
mdbook-mermaid install .
mdbook build
