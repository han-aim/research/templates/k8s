#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
printf \
  '%s\n' \
  '@edge-main https://dl-cdn.alpinelinux.org/alpine/edge/main' \
  >>/etc/apk/repositories
cd \
  -- \
  "/tmp/_template/mdbook/k8s/${DISTRO:?}/"
./install_packages_development.sh
