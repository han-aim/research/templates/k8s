#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
apk \
  update
if [ -n "${DEBUG+1}" ]; then
  xargs \
    -- \
    apk \
    dot \
    <packages_development.txt
fi
xargs \
  -- \
  apk \
  add \
  <packages_development.txt
doas \
  -n \
  -u unprivileged \
  -- \
  cargo \
  install \
  --locked \
  cargo-binstall
doas \
  -n \
  -u unprivileged \
  -- \
  cargo \
  binstall \
  --locked \
  --no-confirm \
  mdbook \
  mdbook-autosummary \
  mdbook-mermaid
