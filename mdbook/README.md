# MdBookify

A template repository for [mdBook](https://rust-lang.github.io/mdBook/)-based websites.

Change the mdBook's title in e.g., the [`.env`](.env.template)-file.

Run

```sh
git update-index --assume-unchanged -- src/SUMMARY.md
```

to avoid committing changes to this generated file.
