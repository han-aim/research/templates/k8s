#!/bin/bash -eux
set -o pipefail
if [ "$1" = 'run' ]; then
  shift
  exec podman run \
    --security-opt label=disable \
    --security-opt unmask=ALL \
    --device=/dev/fuse \
    --device=/dev/net/tun \
    --cap-add=SYS_ADMIN \
    --userns=keep-id \
    "$@"
else
  exec podman "$@"
fi
