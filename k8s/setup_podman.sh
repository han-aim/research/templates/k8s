#!/bin/bash -eux
set -o pipefail
sed \
  -i \
  's/^\#\(mount_program = \"\/usr\/bin\/fuse-overlayfs\"\)/\1/' \
  /etc/containers/storage.conf
usermod \
  --add-subuids '100000-165535' \
  --add-subgids '100000-165535' \
  unprivileged
touch \
  /var/run/docker.sock
chmod \
  644 \
  /etc/containers/containers.conf
sed \
  -i \
  -e 's|^#mount_program|mount_program|g' \
  -e '/additionalimage.*/a "/var/lib/shared",' \
  -e 's/enable_partial_images[[:space:]]*=[[:space:]]"false"/enable_partial_images = "true"/' \
  -e 's|^mountopt[[:space:]]*=.*$|mountopt = "nodev,fsync=0"|g' \
  /etc/containers/storage.conf
mkdir \
  -- \
  /var/lib/shared/
cd \
  -- \
  /var/lib/shared/
install \
  -d \
  -- \
  overlay-images \
  overlay-layers \
  vfs-images \
  vfs-layers
touch \
  overlay-images/images.lock \
  overlay-layers/layers.lock \
  vfs-images/images.lock \
  vfs-layers/layers.lock
install \
  -g unprivileged \
  -o unprivileged \
  -d \
  -D \
  -- \
  ~unprivileged/.local/share/containers
