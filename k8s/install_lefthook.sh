#!/bin/bash -eux
set -o pipefail
version_lefthook='1.8.5'
arch="$(uname -m)"
case "${arch:?}" in
  aarch64)
    archmapped=arm64
    ;;
  x86_64)
    # Not `amd64` - depending on the artifact.
    archmapped=x86_64
    ;;
  *)
    archmapped="${arch:?}"
    ;;
esac
curl \
  --fail \
  --location \
  --output /usr/local/bin/lefthook \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  -- \
  "https://github.com/evilmartians/lefthook/releases/download/v${version_lefthook:?}/lefthook_${version_lefthook:?}_Linux_${archmapped:?}"
chmod \
  a+x \
  -- \
  /usr/local/bin/lefthook
