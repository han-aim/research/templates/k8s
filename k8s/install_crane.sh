#!/bin/bash -eux
set -o pipefail
version_crane='0.20.2'
arch="$(uname -m)"
case "${arch:?}" in
  aarch64)
    archmapped=arm64
    ;;
  x86_64)
    # Not `amd64` - depending on the artifact.
    archmapped=x86_64
    ;;
  *)
    archmapped="${arch:?}"
    ;;
esac
curl \
  --fail \
  --location "https://github.com/google/go-containerregistry/releases/download/v${version_crane:?}/go-containerregistry_Linux_${archmapped:?}.tar.gz" \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  >/tmp/go-containerregistry.tar.gz
tar \
  -z \
  -x \
  -v \
  -f /tmp/go-containerregistry.tar.gz \
  -C /usr/local/bin/ \
  crane
