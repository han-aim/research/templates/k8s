#!/bin/bash -eux
set -o pipefail
[ ! -e /etc/apt/apt.conf.d/docker-clean ] ||
  rm \
    -f \
    -- \
    /etc/apt/apt.conf.d/docker-clean
printf \
  '%s\n' \
  'Binary::apt::APT::Keep-Downloaded-Packages "true";' > \
  /etc/apt/apt.conf.d/keep-cache
printf \
  '%s\n' \
  'debconf debconf/frontend select Noninteractive' |
  debconf-set-selections
apt-get \
  update \
  --yes
apt-get \
  autoremove \
  --yes
cd \
  -- \
  "/tmp/_template/k8s/${DISTRO:?}/"
./install_packages_production.sh
./setup_unix_auth.sh \
  unprivileged
install \
  -d \
  -g unprivileged \
  -o unprivileged \
  -- \
  '/opt/app/' \
  '/opt/app/bin/'
cp \
  -p \
  -- \
  '/tmp/_template/k8s/setup_rrsync.sh' \
  '/usr/local/bin/'
