#!/bin/bash -eux
set -o pipefail
export UV_COMPILE_BYTECODE=1
export UV_NO_CACHE=1
export UV_NO_INSTALLER_METADATA=1
/tmp/_template/python/k8s/install_uv.sh
export PDM_NO_CACHE=1
export PDM_NON_INTERACTIVE=1
/tmp/_template/python/k8s/install_pdm.sh
cd \
  -- \
  /opt/app/
if [ -z "${VIRTUAL_ENV+x}" ]; then
  pdm \
    venv \
    create \
    --with-pip 3.12 \
    --system-site-packages
fi
pdm \
  info
pdm \
  info \
  --env
pdm \
  cache \
  info
pdm \
  list \
  --graph
pdm \
  lock \
  --check
pdm \
  install \
  --dev \
  --fail-fast \
  --frozen-lockfile \
  --no-editable \
  --no-isolation \
  --no-self \
  --verbose
