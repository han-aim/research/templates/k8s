# syntax=docker.io/docker/dockerfile:1@sha256:ac85f380a63b13dfcefa89046420e1781752bab202122f8f50032edf31be0021
ARG DISTRO=ubuntu
ARG REGISTRY_IMAGE_BASE
ARG VERSION_DISTRO=24.04
FROM ${REGISTRY_IMAGE_BASE}${DISTRO:?}:${VERSION_DISTRO:?} AS production
ARG DEBIAN_FRONTEND=noninteractive
ARG DISTRO=ubuntu
ARG SELINUXRELABEL=
ARG SOURCE_DATE_EPOCH
ARG REVISION
ARG TARGETARCH
ARG VERSION_DISTRO=24.04
WORKDIR /opt/app/
ENV \
  PATH="/opt/app/bin:/home/unprivileged/.local/bin:/usr/local/bin:${PATH:?}"
ENTRYPOINT ["/usr/bin/tini", "-g"]
WORKDIR /opt/app/
ARG _INPUTPATH_1=_template/k8s/${DISTRO:?}/packages_production_1.txt
ARG _INPUTPATH_2=_template/k8s/${DISTRO:?}/packages_production_2.txt
ARG _INPUTPATH_3=_template/k8s/${DISTRO:?}/install_packages_production.sh
ARG _INPUTPATH_4=_template/k8s/${DISTRO:?}/setup_unix_auth.sh
ARG _INPUTPATH_5=_template/k8s/${DISTRO:?}/production.sh
ARG _INPUTPATH_6=_template/k8s/setup_rrsync.sh
RUN \
  --mount=type=tmpfs,destination=/tmp/,tmpfs-size=128m \
  --mount=type=bind,source=${_INPUTPATH_1:?},target=/tmp/${_INPUTPATH_1:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_2:?},target=/tmp/${_INPUTPATH_2:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_3:?},target=/tmp/${_INPUTPATH_3:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_4:?},target=/tmp/${_INPUTPATH_4:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_5:?},target=/tmp/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_6:?},target=/tmp/${_INPUTPATH_6:?}${SELINUXRELABEL} \
  --mount=type=cache,id=packages-${DISTRO:?}-${VERSION_DISTRO:?}-${TARGETARCH},target=/var/cache/apt/,sharing=locked \
  "/tmp/${_INPUTPATH_5:?}"
USER unprivileged
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="${DISTRO:?}:${VERSION_DISTRO:?}" \
  org.opencontainers.image.description="Serves as application base image for specific platforms." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="${DISTRO:?} ${VERSION_DISTRO:?} application base image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
FROM production AS development
ARG DEBIAN_FRONTEND=noninteractive
ARG DISTRO=ubuntu
ENV PNPM_HOME="/home/unprivileged/.local/share/pnpm"
ARG SELINUXRELABEL=
ARG SOURCE_DATE_EPOCH
ARG TARGETARCH
ARG VERSION_DISTRO=24.04
ARG REVISION
ENV PATH="${PNPM_HOME:?}:/home/unprivileged/.cargo/bin:${PATH:?}"
USER 0
ARG _INPUTPATH_1=_template/k8s/${DISTRO:?}/development.sh
ARG _INPUTPATH_2=_template/k8s/${DISTRO:?}/install_packages_development.sh
ARG _INPUTPATH_3=_template/k8s/${DISTRO:?}/install_python_tools.sh
ARG _INPUTPATH_4=_template/k8s/${DISTRO:?}/packages_development.txt
ARG _INPUTPATH_5=_template/k8s/install_lefthook.sh
ARG _INPUTPATH_6=_template/python/k8s/install_uv.sh
ARG _INPUTPATH_7=_template/python/k8s/install_pdm.sh
ARG _INPUTPATH_8=pyproject.toml
ARG _INPUTPATH_9=README.md
ARG _INPUTPATH_10=pdm.lock
ARG _INPUTPATH_11=_template/k8s/install_crane.sh
COPY ${_INPUTPATH_8:?} /opt/app/${_INPUTPATH_8:?}
RUN \
  --mount=type=tmpfs,destination=/tmp/,tmpfs-size=128m \
  --mount=type=bind,source=${_INPUTPATH_1:?},target=/tmp/${_INPUTPATH_1:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_2:?},target=/tmp/${_INPUTPATH_2:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_3:?},target=/tmp/${_INPUTPATH_3:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_4:?},target=/tmp/${_INPUTPATH_4:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_5:?},target=/tmp/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_6:?},target=/tmp/${_INPUTPATH_6:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_7:?},target=/tmp/${_INPUTPATH_7:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_9:?},target=/opt/app/${_INPUTPATH_9:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_10:?},target=/opt/app/${_INPUTPATH_10:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_11:?},target=/tmp/${_INPUTPATH_11:?}${SELINUXRELABEL} \
  --mount=type=cache,id=packages-${DISTRO:?}-${VERSION_DISTRO:?}-${TARGETARCH},target=/var/cache/apk/,sharing=locked \
  "/tmp/${_INPUTPATH_1:?}"
USER unprivileged
WORKDIR /home/unprivileged
COPY \
  --chown=unprivileged:unprivileged \
  _template/k8s/docker.sh \
  .local/bin/docker
WORKDIR /opt/app/
ENV \
  VIRTUAL_ENV=/opt/app/.venv
ENV \
  PATH="${VIRTUAL_ENV:?}/bin/:${PATH:?}"
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="k8s-${DISTRO:?}:production--${REVISION:?}" \
  org.opencontainers.image.description="Serves as a development base image for specific platforms." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="${DISTRO:?} ${VERSION_DISTRO:?} development base image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
