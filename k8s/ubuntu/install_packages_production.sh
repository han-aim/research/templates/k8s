#!/bin/bash -eux
set -o pipefail
xargs \
  -- \
  apt-get \
  install \
  --no-install-recommends \
  --no-upgrade \
  --yes \
  <packages_production_1.txt
curl \
  --fail \
  --location \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  -- \
  https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key |
  gpg \
    --dearmor \
    --output /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
printf 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' |
  tee /etc/apt/sources.list.d/kubernetes.list
apt-get \
  update \
  --yes
xargs \
  -- \
  apt-get \
  install \
  --no-install-recommends \
  --no-upgrade \
  --yes \
  <packages_production_2.txt
