#!/bin/bash -eux
set -o pipefail
xargs \
  -- \
  apt-get \
  install \
  --no-install-recommends \
  --no-upgrade \
  --yes \
  <packages_development.txt
curl \
  --fail \
  --location \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  -- \
  https://deb.nodesource.com/setup_current.x |
  bash -
apt-get \
  install \
  --no-install-recommends \
  --no-upgrade \
  --yes \
  nodejs
curl \
  --fail \
  --location \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  -- \
  https://get.pnpm.io/install.sh |
  env \
    ENV='/dev/null' \
    SHELL='/bin/sh' \
    doas \
    -n \
    -u unprivileged \
    -- \
    bash -
doas \
  -n \
  -u unprivileged \
  -- \
  pnpm \
  add \
  --global \
  markdownlint-cli2 \
  prettier
