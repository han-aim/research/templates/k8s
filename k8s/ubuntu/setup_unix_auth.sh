#!/bin/bash -eux
set -o pipefail
groupadd \
  --gid 10001 \
  --system \
  -- \
  "$1"
useradd \
  --create-home \
  --gid "$1" \
  --no-log-init \
  --system \
  --uid 10001 \
  -- \
  "$1"
printf \
  '%s\n' \
  'permit nopass keepenv setenv { PATH SHELL } root as root' \
  'permit nopass keepenv setenv { PATH SHELL } root as '"$1" \
  >>/etc/doas.conf
doas \
  -C \
  /etc/doas.conf
