#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
cd \
  -- \
  "/tmp/_template/k8s/${DISTRO:?}/"
./install_packages_development.sh
doas \
  -n \
  -u unprivileged \
  -- \
  ./install_python_tools.sh
../install_lefthook.sh
../install_crane.sh
printf \
  '%s\n' \
  'permit nopass keepenv setenv { PATH SHELL } unprivileged as root' \
  >>/etc/doas.d/doas.conf
