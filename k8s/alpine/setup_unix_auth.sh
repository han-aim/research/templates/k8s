#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
addgroup \
  -g 10001 \
  -S \
  -- \
  "$1"
adduser \
  -D \
  -G "$1" \
  -S \
  -u 10001 \
  -- \
  "$1"
printf \
  '%s\n' \
  'permit nopass keepenv setenv { PATH SHELL } root as root' \
  'permit nopass keepenv setenv { PATH SHELL } root as '"$1" \
  >>/etc/doas.d/doas.conf
doas \
  -C \
  /etc/doas.d/doas.conf
