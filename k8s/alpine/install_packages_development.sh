#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
apk \
  update
if [ -n "${DEBUG+1}" ]; then
  xargs \
    -- \
    apk \
    dot \
    <packages_development.txt
fi
xargs \
  -- \
  apk \
  add \
  --virtual \
  development \
  <packages_development.txt
curl \
  --fail \
  --location \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  -- \
  https://get.pnpm.io/install.sh |
  env \
    ENV='/dev/null' \
    SHELL='/bin/sh' \
    doas \
    -n \
    -u unprivileged \
    -- \
    bash -
doas \
  -n \
  -u unprivileged \
  -- \
  pnpm \
  add \
  --global \
  markdownlint-cli2 \
  prettier
