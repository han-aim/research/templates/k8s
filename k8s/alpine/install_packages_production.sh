#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
apk \
  update
if [ -n "${DEBUG+1}" ]; then
  xargs \
    -- \
    apk \
    dot \
    <packages_production_1.txt
fi
xargs \
  -- \
  apk \
  add \
  --virtual \
  production_1 \
  <packages_production_1.txt
apk \
  update \
  --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community
if [ -n "${DEBUG+1}" ]; then
  xargs \
    -- \
    apk \
    dot \
    <packages_production_2.txt
fi
xargs \
  -- \
  apk \
  add \
  --virtual \
  production_2 \
  <packages_production_2.txt
