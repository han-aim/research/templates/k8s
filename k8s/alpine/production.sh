#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
cd \
  -- \
  "/tmp/_template/k8s/${DISTRO:?}/"
./install_packages_production.sh
./setup_unix_auth.sh \
  unprivileged
install \
  -d \
  -g unprivileged \
  -o unprivileged \
  -- \
  '/opt/app/' \
  '/opt/app/bin/'
cp \
  -p \
  -- \
  '/tmp/_template/k8s/setup_rrsync.sh' \
  '/usr/local/bin/'
