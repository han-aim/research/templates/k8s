#!/bin/sh
set -eux
cd \
  -- \
  /run/ssh
install \
  -d \
  -g unprivileged \
  -m 0700 \
  -o unprivileged \
  -- \
  .ssh
cd \
  -- \
  .ssh
install \
  -g unprivileged \
  -m 600 \
  -o unprivileged \
  -v \
  -- \
  /run/secrets/identity-sshd/identity.pub \
  authorized_keys
install \
  -g unprivileged \
  -m 600 \
  -o unprivileged \
  -v \
  -- \
  /run/secrets/hostkey-sshd/ssh_host_ed25519_key \
  ssh_host_ed25519_key
/usr/sbin/sshd \
  -G \
  -T
