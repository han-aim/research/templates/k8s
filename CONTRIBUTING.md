# Contributing<!-- omit in toc -->

Please follow the processes defined in the team guidelines.

## How to edit the Markdown documents

<!-- markdown-link-check-disable-next-line -->

See [`src/`](src/) or [this project's website](https://han-aim.gitlab.io/research/guidelines).

- Use the [Markdown All in One extension](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) to maintain tables of contents.
