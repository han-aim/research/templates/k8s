#!/bin/bash -eux
# inherit DEBUG and RETURN trap for functions
set -T
# prevent file overwrite by > &> <>
set -C
# inherit -e
set -E
# exit immediately on faults
set -e
# exit on pipe failure
set -o pipefail
export PATH="${HOME:?}/.local/bin:${PATH:?}"
export PYTHONDONTWRITEBYTECODE=1
# Required for musl libc
export UV_PYTHON_PREFERENCE=system
printf \
  -- \
  '%b' \
  "${GLOBS_GIT_RELEVANT_FILES:?}" |
  xargs \
    -0 \
    -- \
    git \
    ls-files \
    --deduplicate \
    --error-unmatch \
    --exclude-standard \
    -z \
    -- |
  lefthook \
    --no-tty \
    -vvv \
    run \
    "${1:?}" \
    --files-from-stdin
