#!/bin/bash -eux
# inherit DEBUG and RETURN trap for functions
set -T
# prevent file overwrite by > &> <>
set -C
# inherit -e
set -E
# exit immediately on faults
set -e
# exit on pipe failure
set -o pipefail
export PYTHONDONTWRITEBYTECODE=1
# shellcheck disable=SC1091
source \
  .venv/bin/activate
python \
  -m aimtools \
  --path_dir_output . \
  container \
  build \
  --tool podman
