#!/bin/bash -eux
# inherit DEBUG and RETURN trap for functions
set -T
# prevent file overwrite by > &> <>
set -C
# inherit -e
set -E
# exit immediately on faults
set -e
# exit on pipe failure
set -o pipefail
export PYTHONDONTWRITEBYTECODE=1
# Required for musl libc
export UV_PYTHON_PREFERENCE=system
# TODO: Provide Kubernetes configuration and .env file somewhere.
python3 \
  -m aimtools \
  --path_dir_output . \
  kubernetes \
  debug
