#!/bin/bash -eux
# inherit DEBUG and RETURN trap for functions
set -T
# prevent file overwrite by > &> <>
set -C
# inherit -e
set -E
# exit immediately on faults
set -e
# exit on pipe failure
set -o pipefail
export PYTHONDONTWRITEBYTECODE=1
export UV_COMPILE_BYTECODE=0
export UV_NO_CACHE=1
export UV_NO_INSTALLER_METADATA=1
export UV_PYTHON_PREFERENCE=only-managed
if [ -n "${VIRTUAL_ENV:=}" ]; then
  # shellcheck disable=SC1091
  source "${VIRTUAL_ENV:=.}"/bin/activate
  deactivate
fi
export PATH="${HOME:?}/.local/bin:${PATH:?}"
_template/python/k8s/install_uv.sh
uv \
  python \
  install
uv \
  venv \
  --system-site-packages
# shellcheck disable=SC1091
source \
  .venv/bin/activate
uv \
  pip \
  install \
  --extra-index-url https://gitlab.com/api/v4/projects/50625041/packages/pypi/simple \
  --only-binary ':all:' \
  --upgrade \
  aimtools
python \
  -m aimtools \
  --path_dir_output . \
  container \
  login \
  --tool podman
python \
  -m aimtools \
  --path_dir_output . \
  container \
  manifest \
  --tool podman \
  amd64 \
  arm64
