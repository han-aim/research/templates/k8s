#!/bin/bash -eux
# inherit DEBUG and RETURN trap for functions
set -T
# prevent file overwrite by > &> <>
set -C
# inherit -e
set -E
# exit immediately on faults
set -e
# exit on pipe failure
set -o pipefail
dnf \
  install \
  --assumeyes \
  git-core
export PYTHONDONTWRITEBYTECODE=1
export UV_COMPILE_BYTECODE=0
export UV_NO_CACHE=1
export UV_NO_INSTALLER_METADATA=1
export UV_PYTHON_PREFERENCE=only-managed
export PATH="${HOME:?}/.local/bin:${PATH:?}"
_template/python/k8s/install_uv.sh
uv \
  python \
  install
uv \
  venv
ispurelib="$(python -c \
  'from pathlib import Path; \
  import tomllib; \
  print(tomllib.load(Path("pyproject.toml").open(mode="rb"))["tool"]["pdm"]["build"]["is-purelib"])')"
# shellcheck disable=SC1091
source \
  .venv/bin/activate
uv \
  pip \
  install \
  --extra-index-url https://gitlab.com/api/v4/projects/50625041/packages/pypi/simple \
  --only-binary ':all:' \
  --upgrade \
  aimtools
_template/python/k8s/install_pdm.sh
pdm \
  install \
  --group :all
PDM_BUILD_SCM_VERSION="$(pdm run python -m aimtools --path_dir_output . product revision)"
export PDM_BUILD_SCM_VERSION
printf \
  "Check whether revision has been set ... It’s: %s.\n" \
  "${PDM_BUILD_SCM_VERSION:?}"
if [ "$ispurelib" = "True" ] && [ "${RELEASEPLATFORM:?}" = "purepython" ]; then
  pdm \
    build
  pdm \
    --verbose \
    --verbose \
    --verbose \
    publish \
    --no-build
elif [ "$ispurelib" = "False" ] && [ "${RELEASEPLATFORM:?}" = "notpurepython" ]; then
  export CIBW_BEFORE_ALL='uname -a'
  export CIBW_BUILD_FRONTEND='build[uv]'
  cibuildwheel
  pdm \
    --verbose \
    --verbose \
    --verbose \
    publish \
    --dest wheelhouse \
    --no-build
else
  printf \
    '%s.\n' \
    "Job target platform and product target platform mismatch. End this job. "
fi
