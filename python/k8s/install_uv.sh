#!/bin/bash -eux
set -o pipefail
path_file_script="$(mktemp)"
version_uv='0.5.7'
curl \
  --fail \
  --location "https://astral.sh/uv/${version_uv:?}/install.sh" \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  >"${path_file_script:?}"
sh \
  -- \
  "${path_file_script:?}"
