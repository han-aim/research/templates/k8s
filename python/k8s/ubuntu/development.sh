#!/bin/bash -eux
set -o pipefail
cd \
  -- \
  "/tmp/_template/python/k8s/${DISTRO:?}/"
printf \
  '%s\n' \
  'debconf debconf/frontend select Noninteractive' |
  debconf-set-selections
apt-get \
  update \
  --yes
apt-get \
  autoremove \
  --yes
./install_packages_development.sh
doas \
  -n \
  -u unprivileged \
  -- \
  /tmp/_template/python/k8s/install_uv.sh
doas \
  -n \
  -u unprivileged \
  -- \
  /tmp/_template/python/k8s/install_pdm.sh
