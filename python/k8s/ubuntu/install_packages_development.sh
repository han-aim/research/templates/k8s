#!/bin/bash -eux
set -o pipefail
xargs \
  -- \
  apt-get \
  install \
  --no-install-recommends \
  --no-upgrade \
  --yes \
  <packages_development.txt
