#!/bin/bash -eux
set -o pipefail
export PDM_NO_CACHE=1
export PDM_NON_INTERACTIVE=1
if [ -z "${VIRTUAL_ENV+x}" ]; then
  pdm \
    venv \
    create \
    3.12 \
    --system-site-packages
fi
pdm \
  info
pdm \
  info \
  --env
pdm \
  cache \
  info
pdm \
  list \
  --graph
pdm \
  lock \
  --check
pdm \
  install \
  --dev \
  --fail-fast \
  --frozen-lockfile \
  --group build \
  --no-editable \
  --no-isolation \
  --no-self \
  --verbose
pdm \
  install \
  --fail-fast \
  --frozen-lockfile \
  --no-editable \
  --no-isolation \
  --production \
  --verbose
