#!/bin/bash -eux
set -o pipefail
path_file_script="$(mktemp)"
version_pdm='2.22.0'
export PDM_NO_CACHE=1
export PDM_NON_INTERACTIVE=1
curl \
  --fail \
  --location 'https://raw.githubusercontent.com/pdm-project/pdm/main/install-pdm.py' \
  --proto '=https' \
  --show-error \
  --silent \
  --tlsv1.3 \
  >"${path_file_script:?}"
python3 \
  -- \
  "${path_file_script:?}" \
  -v "v${version_pdm:?}"
