#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
export PDM_NO_CACHE=1
export PDM_NON_INTERACTIVE=1
pdm \
  info
pdm \
  info \
  --env
pdm \
  cache \
  info
pdm \
  list \
  --graph
pdm \
  lock \
  --check
pdm \
  install \
  --dev \
  --fail-fast \
  --frozen-lockfile \
  --group build \
  --no-editable \
  --no-isolation \
  --no-self \
  --verbose
pdm \
  install \
  --dev \
  --fail-fast \
  --frozen-lockfile \
  --no-editable \
  --no-isolation \
  --no-self \
  --verbose
