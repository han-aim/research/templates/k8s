#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
cd \
  -- \
  "/tmp/_template/python/k8s/${DISTRO:?}/"
./install_packages_development.sh
doas \
  -n \
  -u unprivileged \
  -- \
  /tmp/_template/python/k8s/install_uv.sh
doas \
  -n \
  -u unprivileged \
  -- \
  /tmp/_template/python/k8s/install_pdm.sh
