#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
apk \
  update
if [ -n "${DEBUG+1}" ]; then
  xargs \
    -- \
    apk \
    dot \
    <packages_development.txt
fi
xargs \
  -- \
  apk \
  add \
  --virtual \
  development-python \
  <packages_development.txt
