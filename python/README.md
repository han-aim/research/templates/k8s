# Python product template<!-- omit in toc -->

- [Features](#features)
  - [`src/`](#src)
  - [Base template: `../`](#base-template-)
  - [`.sourcery.yaml`](#sourceryyaml)
  - [`pdm.toml` and `pyproject.toml`](#pdmtoml-and-pyprojecttoml)
  - [`renovate.json`](#renovatejson)
- [To use](#to-use)
  - [Adapt to your product](#adapt-to-your-product)

## Features

This section will relate concrete files in the template to functionality.
There much more to this template than just the files you see.
The files configure various tools that are called through this template, but often their default, implicit configuration suffices.

### `src/`

Source directory for your Python modules.

### Base template: `../`

See [`../README.md`](../README.md).
This template is a superset of the main template.

### `.sourcery.yaml`

Sourcery assists Python programmers to improve their work with specific machine learning-based recommendations and automatic fixes for their actual source code.

See [Welcome - Sourcery Documentation](https://docs.sourcery.ai/Welcome/).

### `pdm.toml` and `pyproject.toml`

PDM is an all-in-one tool that manages your Python-based product's source code and helps you deliver your product to users.
For example, it helps you specify your dependencies and metadata.

See [Introduction - PDM](https://pdm.fming.dev/latest/).

### `renovate.json`

Renovate is a software system, often called a bot, that automatically maintains your source code, focusing on rote tasks such as keep dependencies up to date.

See [Renovate Docs](https://docs.renovatebot.com/).

## To use

Please refer to [`../README.md` - To use](../README.md#to-use).
On top of that, follow the next instructions.

### Adapt to your product

1. [ ] The product name (lowercase, without spaces) as PDM ‘project name’ in [`pyproject.toml`](pyproject.toml).
