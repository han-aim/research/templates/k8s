#!/bin/bash -eux
set -o pipefail
env \
  RUSTFLAGS="${RUSTFLAGS:=} -C target-feature=-crt-static" \
  doas \
  -u unprivileged \
  -- \
  cargo \
  install \
  --locked \
  cargo-binstall
env \
  RUSTFLAGS="${RUSTFLAGS:=} -C target-feature=-crt-static" \
  doas \
  -u unprivileged \
  -- \
  cargo \
  binstall \
  --locked \
  --no-confirm \
  cargo-binutils \
  cargo-edit \
  cargo-fuzz \
  cargo-generate \
  cargo-nextest \
  flamegraph
