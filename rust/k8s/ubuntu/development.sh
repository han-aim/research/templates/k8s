#!/bin/bash -eux
set -o pipefail
printf \
  '%s\n' \
  'debconf debconf/frontend select Noninteractive' |
  debconf-set-selections
apt-get \
  update \
  --yes
apt-get \
  autoremove \
  --yes
cd \
  -- \
  "/tmp/_template/rust/k8s/${DISTRO:?}/"
./install_packages_development.sh
curl \
  --fail \
  --proto '=https' \
  --tlsv1.2 \
  --show-error \
  --silent \
  -- \
  https://apt.llvm.org/llvm.sh |
  bash \
    -s \
    -- \
    18
../setup_rust.sh
curl \
  --fail \
  --location \
  --output-dir /var/cache/apt/archives/ \
  --proto '=https' \
  --tlsv1.3 \
  --remote-name \
  --show-error \
  --silent \
  -- \
  "https://github.com/rr-debugger/rr/releases/download/5.8.0/rr-5.8.0-Linux-$(uname -m).deb"
dpkg \
  --install \
  "/var/cache/apt/archives/rr-5.8.0-Linux-$(uname -m).deb"
../setup_rust_development.sh
