#!/bin/bash -eux
set -o pipefail
case "$(lsb_release --id --short)" in
  Alpine)
    _env=musl
    doas \
      -u unprivileged \
      -- \
      rustup-init \
      --default-host "$(uname -m)-unknown-linux-${_env}" \
      --default-toolchain 1.79 \
      --no-modify-path \
      -y
    ;;
  Ubuntu)
    _env=gnu
    curl \
      --fail \
      --proto '=https' \
      --tlsv1.3 \
      --show-error \
      --silent \
      -- \
      https://sh.rustup.rs |
      doas \
        -n \
        -u unprivileged \
        -- \
        sh \
        -s \
        -- \
        --default-host "$(uname -m)-unknown-linux-${_env}" \
        --default-toolchain 1.79 \
        --no-modify-path \
        -y
    ;;
  *)
    printf '%s' 'Unsupported operating system.'
    exit 1
    ;;
esac
doas \
  -u unprivileged \
  -- \
  rustup \
  toolchain \
  install \
  nightly \
  --component \
  cargo \
  clippy \
  llvm-tools \
  miri \
  rust-analysis \
  rust-docs \
  rust-src \
  rust-std \
  rustc \
  rustfmt \
  --no-self-update \
  --target "$(uname -m)-unknown-linux-${_env}"
doas \
  -u unprivileged \
  -- \
  rustup \
  set \
  auto-self-update \
  disable
