#!/bin/ash -eux
# shellcheck shell=bash
set -o pipefail
cd \
  -- \
  "/tmp/_template/rust/k8s/${DISTRO:?}/"
./install_packages_development.sh
../setup_rust.sh
../setup_rust_development.sh
