# syntax=docker.io/docker/dockerfile:1@sha256:ac85f380a63b13dfcefa89046420e1781752bab202122f8f50032edf31be0021
ARG DISTRO=alpine
ARG REGISTRY_IMAGE_PRODUCT
ARG REVISION
# TODO: Currently a dummy stage, since Cargo and rustc are always required to build production, but is a development tool.
# This should be a scratch image.
FROM ${REGISTRY_IMAGE_PRODUCT}k8s-${DISTRO:?}:production--${REVISION} AS production
ARG DISTRO=alpine
ARG REGISTRY_IMAGE_PRODUCT
ARG REVISION
ARG SOURCE_DATE_EPOCH
ARG TARGETARCH
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="k8s-${DISTRO:?}:production--${REVISION:?}" \
  org.opencontainers.image.description="Serves as base image for Rust applications." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="Rust application base image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
FROM ${REGISTRY_IMAGE_PRODUCT}k8s-${DISTRO:?}:development--${REVISION:?} AS development
ARG DISTRO=alpine
ARG NAME_PRODUCT
ARG REGISTRY_IMAGE_PRODUCT
ARG REVISION
ARG SELINUXRELABEL=
ARG SOURCE_DATE_EPOCH
ARG TARGETARCH
ARG VERSION_DISTRO=3.20
WORKDIR /opt/app/
USER 0
ENV \
  RUST_LOG=info \
  RUSTC_WRAPPER=sccache \
  RUSTFLAGS="${RUSTFLAGS} -C link-arg=-fuse-ld=mold"
RUN \
  install \
  -d \
  -g unprivileged \
  -o unprivileged \
  -- \
  /builds/target/ \
  ~unprivileged/.cache/ \
  ~unprivileged/.cache/miri \
  ~unprivileged/.cache/sccache \
  ~unprivileged/.cargo/ \
  ~unprivileged/.cargo/registry
ARG _INPUTPATH_1=_template/rust/k8s/${DISTRO:?}/development.sh
ARG _INPUTPATH_2=_template/rust/k8s/${DISTRO:?}/packages_development.txt
ARG _INPUTPATH_3=_template/rust/k8s/${DISTRO:?}/install_packages_development.sh
ARG _INPUTPATH_4=_template/rust/k8s/setup_rust_development.sh
ARG _INPUTPATH_5=_template/rust/k8s/setup_rust.sh
RUN \
  --mount=type=tmpfs,destination=/tmp/,tmpfs-size=1g \
  --mount=type=bind,source=${_INPUTPATH_1:?},target=/tmp/${_INPUTPATH_1:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_2:?},target=/tmp/${_INPUTPATH_2:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_3:?},target=/tmp/${_INPUTPATH_3:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_4:?},target=/tmp/${_INPUTPATH_4:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_5:?},target=/tmp/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=cache,id=cargoregistry-${TARGETARCH},target=/home/unprivileged/.cargo/registry,sharing=locked,uid=10001,gid=10001${SELINUXRELABEL} \
  --mount=type=cache,id=cargotarget-${NAME_PRODUCT:?}-${TARGETARCH},sharing=locked,target=/builds/target/,gid=10001,uid=10001${SELINUXRELABEL} \
  --mount=type=cache,id=packages-${DISTRO:?}-${VERSION_DISTRO:?}-${TARGETARCH},target=/var/cache/apk/,sharing=locked \
  --mount=type=cache,id=cache-${TARGETARCH},target=/home/unprivileged/.cache/,sharing=locked,uid=10001,gid=10001${SELINUXRELABEL} \
  --mount=type=cache,id=sccache-${TARGETARCH},target=/home/unprivileged/.cache/sccache/,sharing=locked,uid=10001,gid=10001${SELINUXRELABEL} \
  "/tmp/${_INPUTPATH_1:?}"
USER unprivileged
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="k8s-${DISTRO:?}:development--${REVISION:?}" \
  org.opencontainers.image.description="Serves as base image for Rust development." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="Rust development base image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
