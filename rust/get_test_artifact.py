#!/usr/bin/env python3
import fileinput
import json

with fileinput.input() as stdin:
    for line in stdin:
        message = json.loads(line)
        if message.get("reason") == "compiler-artifact" and message.get("profile").get("test"):
            print(message["executable"])  # noqa: T201
